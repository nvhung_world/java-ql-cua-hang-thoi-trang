-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: fashionshop
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chitietdh`
--

DROP TABLE IF EXISTS `chitietdh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitietdh` (
  `IDCTDH` int(11) NOT NULL AUTO_INCREMENT,
  `MaDH` int(11) NOT NULL,
  `MaMH` int(11) NOT NULL,
  `SoLuong` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IDCTDH`),
  KEY `fk_CTDH_MaDH` (`MaDH`),
  KEY `fk_CTDH_MaMH` (`MaMH`),
  CONSTRAINT `fk_CTDH_MaDH` FOREIGN KEY (`MaDH`) REFERENCES `donhang` (`MaDH`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CTDH_MaMH` FOREIGN KEY (`MaMH`) REFERENCES `mathang` (`MaMH`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietdh`
--

LOCK TABLES `chitietdh` WRITE;
/*!40000 ALTER TABLE `chitietdh` DISABLE KEYS */;
INSERT INTO `chitietdh` VALUES (7,1,6,1),(8,1,5,1),(9,1,3,1),(10,6,18,1),(23,11,1,1),(24,12,2,1),(25,13,4,1),(26,14,5,1),(27,15,2,13),(28,16,3,3),(29,6,7,1);
/*!40000 ALTER TABLE `chitietdh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donhang`
--

DROP TABLE IF EXISTS `donhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donhang` (
  `MaDH` int(11) NOT NULL AUTO_INCREMENT,
  `MaTK` int(11) NOT NULL,
  `NgayBan` date NOT NULL,
  `TrangThai` enum('Giỏ Hàng','Đang Chuyển','Đã Thanh Toán') CHARACTER SET utf8 NOT NULL DEFAULT 'Giỏ Hàng',
  `NoiGiao` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaDH`),
  KEY `fk_DH_MaTK` (`MaTK`),
  CONSTRAINT `fk_DH_MaTK` FOREIGN KEY (`MaTK`) REFERENCES `taikhoan` (`MaTK`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donhang`
--

LOCK TABLES `donhang` WRITE;
/*!40000 ALTER TABLE `donhang` DISABLE KEYS */;
INSERT INTO `donhang` VALUES (1,20,'2016-12-04','Đang Chuyển','                                                                                                    '),(3,20,'2016-12-04','Đang Chuyển','                                                                                    '),(4,20,'2016-12-04','Đang Chuyển','                                                                                    '),(5,20,'2016-12-04','Đang Chuyển','                                                                                    '),(6,20,'2016-12-04','Giỏ Hàng',NULL),(11,0,'2016-12-05','Đã Thanh Toán',NULL),(12,0,'2016-12-05','Đã Thanh Toán',NULL),(13,0,'2016-12-05','Đã Thanh Toán',NULL),(14,0,'2016-12-05','Đã Thanh Toán',NULL),(15,0,'2016-12-05','Đã Thanh Toán',NULL),(16,0,'2016-12-05','Đã Thanh Toán',NULL);
/*!40000 ALTER TABLE `donhang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mathang`
--

DROP TABLE IF EXISTS `mathang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathang` (
  `MaMH` int(11) NOT NULL AUTO_INCREMENT,
  `TenMH` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Loai` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'Không Dõ',
  `TonKho` int(11) NOT NULL DEFAULT '0',
  `GiaBan` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MaMH`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mathang`
--

LOCK TABLES `mathang` WRITE;
/*!40000 ALTER TABLE `mathang` DISABLE KEYS */;
INSERT INTO `mathang` VALUES (1,'Áo khoác Phao','Áo Khoác',10,200000),(2,'Váy Zen','Váy',20,300000),(3,'Váy Ngắn','Váy',30,200000),(4,'Áo khoác thể thao','Áo Khoác',10,200000),(5,'Áo khoác có mũ','Áo Khoác',10,200000),(6,'Áo khoác da','Áo Khoác',10,200000),(7,'Áo thun','Áo',10,200000),(8,'Áo thun không cổ','Áo',10,200000),(9,'Áo polo','Áo',10,200000),(10,'Áo thun dài tay','Áo',10,200000),(11,'Áo len','Áo',10,200000),(12,'Áo sơ mi nam','Áo',10,200000),(13,'Sơ mi dài tay','Áo',10,200000),(14,'Sơ mi ngắn tay','Áo',10,200000),(15,'Quần kaki','Quần',10,200000),(16,'Quần jean','Quần',10,200000),(17,'Quần tây','Quần',10,200000),(18,'Quần lót','Quần',10,200000),(19,'Quần boxer','Quần',10,200000),(20,'Quần shorts','Quần',10,200000),(21,'Vest','Vest',10,200000);
/*!40000 ALTER TABLE `mathang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhacungcap`
--

DROP TABLE IF EXISTS `nhacungcap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nhacungcap` (
  `MaNCC` int(11) NOT NULL AUTO_INCREMENT,
  `TenNCC` varchar(30) CHARACTER SET utf8 NOT NULL,
  `SoDT` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DiaChi` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MoTa` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaNCC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhacungcap`
--

LOCK TABLES `nhacungcap` WRITE;
/*!40000 ALTER TABLE `nhacungcap` DISABLE KEYS */;
/*!40000 ALTER TABLE `nhacungcap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phieunhap`
--

DROP TABLE IF EXISTS `phieunhap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phieunhap` (
  `MaPN` int(11) NOT NULL AUTO_INCREMENT,
  `MaNCC` int(11) NOT NULL,
  `MaMH` int(11) NOT NULL,
  `SoLuong` int(11) NOT NULL DEFAULT '0',
  `DonGia` int(11) NOT NULL DEFAULT '0',
  `NgayNhap` date NOT NULL,
  PRIMARY KEY (`MaPN`),
  KEY `fk_MH_MaMH` (`MaMH`),
  KEY `fk_PN_MaNCC` (`MaNCC`),
  CONSTRAINT `fk_MH_MaMH` FOREIGN KEY (`MaMH`) REFERENCES `mathang` (`MaMH`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_PN_MaNCC` FOREIGN KEY (`MaNCC`) REFERENCES `nhacungcap` (`MaNCC`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phieunhap`
--

LOCK TABLES `phieunhap` WRITE;
/*!40000 ALTER TABLE `phieunhap` DISABLE KEYS */;
/*!40000 ALTER TABLE `phieunhap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taikhoan`
--

DROP TABLE IF EXISTS `taikhoan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taikhoan` (
  `MaTK` int(11) NOT NULL AUTO_INCREMENT,
  `TaiKhoan` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MatKhau` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Quyen` enum('Khách Hàng','Nhân Viên','Quản Trị Viên','Ngưng Hoạt Động') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Khách Hàng',
  `NgayLap` date NOT NULL,
  `MaCap2` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `LastLogin` date DEFAULT NULL,
  PRIMARY KEY (`MaTK`),
  UNIQUE KEY `TaiKhoan` (`TaiKhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taikhoan`
--

LOCK TABLES `taikhoan` WRITE;
/*!40000 ALTER TABLE `taikhoan` DISABLE KEYS */;
INSERT INTO `taikhoan` VALUES (0,'NoName','noname','Khách Hàng','2016-04-12',NULL,NULL,NULL),(8,'admin','admin','Quản Trị Viên','2016-12-03','','',NULL),(15,'AnhHung','hungit','Khách Hàng','2016-03-12',NULL,NULL,NULL),(20,'TuanTuTi','tuan','Khách Hàng','2016-03-12',NULL,NULL,NULL);
/*!40000 ALTER TABLE `taikhoan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thongtinmh`
--

DROP TABLE IF EXISTS `thongtinmh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thongtinmh` (
  `MaMH` int(11) NOT NULL,
  `Avatar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ThuongHieu` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DoiTuong` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'All',
  `Nam` int(11) DEFAULT NULL,
  `DanhGia` tinyint(3) NOT NULL DEFAULT '0',
  `LuotXem` int(11) NOT NULL DEFAULT '0',
  `MoTaNgan` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaMH`),
  CONSTRAINT `fk_TTMH_MaMH` FOREIGN KEY (`MaMH`) REFERENCES `mathang` (`MaMH`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thongtinmh`
--

LOCK TABLES `thongtinmh` WRITE;
/*!40000 ALTER TABLE `thongtinmh` DISABLE KEYS */;
INSERT INTO `thongtinmh` VALUES (1,'fashion\\a (1).jpg','May 10','Nữ',2016,1,25,'x'),(2,'fashion\\a (2).jpg','May 10','Nữ',2016,2,55,'ư'),(3,'fashion\\a (3).jpg','May 10','Nam',2016,3,24,'e'),(4,'fashion\\a (4).jpg','May 10','Nam',2016,4,11,'r'),(5,'fashion\\a (5).jpg','Việt Tiến ','Nam',2016,5,12,'t'),(6,'fashion\\a (6).jpg','Việt Tiến','Nữ',2015,1,11,'y'),(7,'fashion\\a (7).jpg','Việt Tiến','Nữ',2015,2,9,'u'),(8,'fashion\\a (8).jpg','Việt Tiến','Nữ',2015,3,12,'i'),(9,'fashion\\a (9).jpg','BLUE EXCHANGE','Nữ',2015,4,13,'o'),(10,'fashion\\a (10).jpg','BLUE EXCHANGE','Bé Trai',2015,5,10,'p'),(11,'fashion\\a (11).jpg','BLUE EXCHANGE','Bé Trai',2013,1,11,'a'),(12,'fashion\\a (12).jpg','BLUE EXCHANGE','Bé Trai',2013,2,12,'s'),(13,'fashion\\a (13).jpg','Novelty','Bé Trai',2013,3,14,'d'),(14,'fashion\\a (14).jpg','Novelty','Bé Gái',2013,4,15,'f'),(15,'fashion\\a (15).jpg','Novelty','Bé Gái',2013,5,15,'g'),(16,'fashion\\a (16).jpg','PT2000','Bé Gái',2014,1,16,'h'),(17,'fashion\\a (17).jpg','PT2000','Nam',2014,2,18,'j'),(18,'fashion\\a (18).jpg','Ninomaxx','Nam',2014,3,19,'k'),(19,'fashion\\a (1).jpg','Ninomaxx','Nam',2014,4,19,'l'),(20,'fashion\\a (12).jpg','Ninomaxx','Nữ',2016,1,22,'x'),(21,'fashion\\a (13).jpg','Ninomaxx','Nữ',2014,5,21,'z');
/*!40000 ALTER TABLE `thongtinmh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thongtintk`
--

DROP TABLE IF EXISTS `thongtintk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thongtintk` (
  `MaTK` int(11) NOT NULL,
  `Ho` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `Ten` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Avatar` blob,
  `GioiTinh` enum('Nam','Nữ','Không Dõ') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Không Dõ',
  `NgaySinh` date DEFAULT NULL,
  `SoCMT` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `SoDT` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `DiaChi` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaTK`),
  CONSTRAINT `fk_TT_MaTK` FOREIGN KEY (`MaTK`) REFERENCES `taikhoan` (`MaTK`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thongtintk`
--

LOCK TABLES `thongtintk` WRITE;
/*!40000 ALTER TABLE `thongtintk` DISABLE KEYS */;
INSERT INTO `thongtintk` VALUES (0,'No','Name',NULL,'Không Dõ',NULL,NULL,NULL,NULL,NULL),(8,'Nguyễn','Hưng',NULL,'Không Dõ',NULL,'','','',''),(15,'Nguyễn ','Hưng',NULL,'Nam',NULL,NULL,NULL,NULL,NULL),(20,'nguyễn','Tuấn',NULL,'Không Dõ',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `thongtintk` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-07 14:17:32
