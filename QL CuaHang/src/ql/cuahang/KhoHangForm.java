package ql.cuahang;
import MySQL.Connect;
import MySQL.SelectQuery;
import MySQL.Row;
import javax.swing.table.DefaultTableModel;
public class KhoHangForm extends javax.swing.JFrame{
    public boolean isClose;
    private SelectQuery adapte;
    private DefaultTableModel tableMode;
    MatHangFrom MH;
    Connect conn;
    public KhoHangForm() {
        initComponents();
        isClose = false;
        setLocationRelativeTo(null);
    }
    public KhoHangForm(Connect conn) {
        initComponents();
        isClose = false;
        setLocationRelativeTo(null);
        this.conn = conn;
        tableMode = (DefaultTableModel)jTable1.getModel();
        adapte = new SelectQuery(conn);
        CreateAdapte();
    }
    private void CreateAdapte()
    {
        adapte.select.add("mathang.MaMH");
        adapte.select.add("TenMH");
        adapte.select.add("ThuongHieu");
        adapte.select.add("DoiTuong");
        adapte.select.add("loai");
        adapte.select.add("TonKho");
        adapte.select.add("GiaBan");
        adapte.select.add("Nam");
        adapte.select.add("DanhGia");
        adapte.select.add("LuotXem");
        adapte.select.add("MoTaNgan");
        //.....................................................
        adapte.from.add("mathang");
        adapte.from.add("thongtinmh");
        //.....................................................
        adapte.where.add("mathang.MaMH = thongtinmh.MaMH");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox4 = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Quản Lý Kho Hàng");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Loại");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả", "Áo", "Quần", "Giầy", "Áo Khoác", "Váy", "Đồ Lót" }));

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mặc Định", "Tên A - Z", "Tên Z - A", "Giá Tăng Giần", "Giá Giảm Giần" }));

        jLabel4.setText("Xếp theo");

        jButton2.setText("Tìm Kiếm");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TimKiemClick(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã mặt hàng", "Tên mặt hàng", "Thuong hiệu", "Đối tượng", "Loại", "Tồn kho", "Giá bán lẻ", "Năm", "Đánh giá", "Lượt xem", "Mô tả"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true, true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel2.setText("Tên MH");

        jLabel6.setText("Mã MH");

        jButton1.setText("Chỉnh sửa");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ChinhSuaClick(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 207, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Close();
    }//GEN-LAST:event_formWindowClosing
    public void Close()
    {
        isClose = true;
        adapte.Close();
        if(MH != null && MH.isClose == false)
            MH.Close();
        dispose();
    }
    private void TimKiemClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TimKiemClick
        // Tạo Query
        adapte.other = "";
        if(!"".equals(jTextField3.getText()))
            adapte.other += " and mathang.MaMH like N'%" + jTextField3.getText() + "%'";
        if(!"".equals(jTextField1.getText()))
            adapte.other += " and TenMH like N'%" + jTextField1.getText() + "%'";
        if(jComboBox1.getSelectedIndex() != 0)
            adapte.other += " and Loai = N'" + jComboBox1.getSelectedItem() + "'";
        //if(jComboBox3.getSelectedIndex() != 0)
            //adapte.other += " and ThuongHieu = N'" + jComboBox1.getSelectedItem() + "'";
        switch (jComboBox4.getSelectedIndex()) {
            case 1:
                adapte.other += " ORDER BY TenMH ASC";
                break;
            case 2:
                adapte.other += " ORDER BY TenMH DESC";
                break;
            case 3:
                adapte.other += " ORDER BY GiaBan ASC";
                break;
            case 4:
                adapte.other += " ORDER BY GiaBan DESC";
                break;
        }
        // xóa tất cả dữ liệu trong bảng
        while(tableMode.getRowCount() > 0)
        {            
            tableMode.removeRow(0);
        }
        // Load Data
        LoadData();
    }//GEN-LAST:event_TimKiemClick

    private void ChinhSuaClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ChinhSuaClick
        if(MH == null || MH.isClose)
        {
            MH = new MatHangFrom(conn);
            MH.setVisible(true);
        }
        else 
            MH.Close();
    }//GEN-LAST:event_ChinhSuaClick
    private  void LoadData()
    {
        adapte.Read();
        for(Row r : adapte.table){
            tableMode.addRow(r.GetAllValue());
        }
    }
            
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField3;
    // End of variables declaration//GEN-END:variables
}
