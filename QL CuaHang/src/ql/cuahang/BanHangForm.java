package ql.cuahang;
import MySQL.Connect;
import MySQL.SelectQuery;
import MySQL.Row;
import java.util.ArrayList;
import java.awt.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
public class BanHangForm extends javax.swing.JFrame implements TableModelListener{
    public boolean isClose;
    private SelectQuery KhAdapte, MhAdapte;
    private DefaultTableModel tableMode;
    Connect conn;
    String TaiKhoan;
    public BanHangForm() {
        initComponents();
    }
    public BanHangForm(Connect conn) 
    {
        initComponents();
        isClose = false;
        setLocationRelativeTo(null);
        tableMode = (DefaultTableModel)jTable1.getModel();
        tableMode.addTableModelListener(this);
        this.conn = conn;
        TaiKhoan = "0";
        KhAdapte = new SelectQuery(conn);
        MhAdapte = new SelectQuery(conn);
        SetupAdapte();
    }
    private void SetupAdapte()
    {
        KhAdapte.select.add("taikhoan.MaTK");
        KhAdapte.select.add("concat(Ho, ' ', Ten) as 'Ho ten'");
        KhAdapte.from.add("taikhoan");
        KhAdapte.from.add("thongtintk");
        KhAdapte.where.add("taikhoan.MaTK = thongtintk.MaTK");
        //........................................................
        MhAdapte.select.add("GiaBan");
        MhAdapte.select.add("TonKho");
        MhAdapte.select.add("TenMH");
        MhAdapte.from.add("mathang");
        MhAdapte.from.add("thongtinmh");
        MhAdapte.where.add("mathang.MaMH = thongtinmh.MaMH");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        StatusLable = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Bán Hàng");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        jLabel2.setText("Tên Khách Hàng");

        jTextField1.setText("NoName");
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                MaKHEditer(evt);
            }
        });

        jLabel1.setText("Tài Khoản Khách Hàng");

        jTextField2.setEditable(false);
        jTextField2.setBackground(new java.awt.Color(255, 204, 102));
        jTextField2.setText("No Name");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "Mã Mặt Hàng", "Số Lượng", "Đơn Giá", "Tồn Kho", "Tên Hàng", "Trại Thái"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel3.setText("Tổng Tiền");

        jTextField3.setEditable(false);
        jTextField3.setBackground(new java.awt.Color(204, 204, 255));
        jTextField3.setText("0");
        jTextField3.setToolTipText("");

        jLabel4.setText("Tiền Nhận");

        jTextField4.setText("0");
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TienNhanEditer(evt);
            }
        });

        jLabel5.setText("Tiền Trả lại");

        jTextField5.setEditable(false);
        jTextField5.setBackground(new java.awt.Color(204, 204, 255));
        jTextField5.setText("0");

        jButton1.setText("Tạo Hóa Đơn");
        jButton1.setEnabled(false);
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TaoHoaDonClick(evt);
            }
        });

        jButton2.setText("Làm mới");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                HuyClick(evt);
            }
        });

        StatusLable.setText("Status");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(29, 29, 29)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(StatusLable)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(StatusLable))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Close();
    }//GEN-LAST:event_formWindowClosing

    private void MaKHEditer(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MaKHEditer
        KhAdapte.other = " and upper(TaiKhoan) = upper(N'" + jTextField1.getText() + "')";
        KhAdapte.Read();
        if(KhAdapte.table.isEmpty())
        {
            TaiKhoan = "0";
            jTextField2.setText("No Name");
            SetStatus("Lỗi: Không tìm thấy khách hàng", false);
        }
        else
        {
            TaiKhoan = KhAdapte.table.get(0).GetValue(0).toString();
            jTextField2.setText(KhAdapte.table.get(0).GetValue(1).toString());
            SetStatus("tìm thấy", true);
        }
    }//GEN-LAST:event_MaKHEditer

    private void TienNhanEditer(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TienNhanEditer
        float TienNhan = 0;
        float TongTien = 0;
        try{
            TongTien = Float.parseFloat(jTextField3.getText());
            TienNhan = Float.parseFloat(jTextField4.getText());
            if(TongTien<=TienNhan)
                jButton1.setEnabled(true);
            else
                jButton1.setEnabled(false);
        }
        catch(NumberFormatException ex)
        {
            SetStatus("Lỗi: Tiền nhận phải là số nguyên dương", false);
        }
        jTextField5.setText("" +(TienNhan - TongTien));
    }//GEN-LAST:event_TienNhanEditer

    private void HuyClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_HuyClick
        tableMode.removeTableModelListener(this);
        while(tableMode.getRowCount() > 1)
            tableMode.removeRow(0);
        tableMode.addTableModelListener(this);
        TaiKhoan = "0";
        jTextField1.setText("No Name");
        jTextField2.setText("No Name");
        jTextField3.setText("0");
        jTextField4.setText("0");
        jTextField5.setText("0");
    }//GEN-LAST:event_HuyClick

    private void TaoHoaDonClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TaoHoaDonClick
        String Query[] = new String[tableMode.getRowCount() + 1];
        Query[0] = "INSERT INTO donhang(MaTK, NgayBan, TrangThai)VALUES(" + TaiKhoan +
                ", '" + LocalDate.now().format(DateTimeFormatter.ISO_DATE) +
                "', 'Đã Thanh Toán');";
        Query[1] = "SET @last_id_in_table1 = LAST_INSERT_ID();";
        for(int i = 0; i< tableMode.getRowCount();i ++)
            if(tableMode.getValueAt(i, 5) != null && "Khả Thi".equals(tableMode.getValueAt(i, 5).toString()))
            {
                String SoLuong = tableMode.getValueAt(i, 1).toString();
                String MaMH = tableMode.getValueAt(i, 0).toString();
                Query[2+i]  ="INSERT INTO chitietdh(MaDH,MaMH,SoLuong)VALUES(@last_id_in_table1, "+MaMH +", "+SoLuong+" );";
            }
        if(conn.MultiExecute(Query) != null)
        {
            HuyClick(null);
        }
        else
        {
            JOptionPane.showConfirmDialog(
            this,
            "Không thành công",
            "Lỗi",
            JOptionPane.OK_OPTION,
            JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_TaoHoaDonClick
    public void Close()
    {
        isClose = true;
        KhAdapte.Close();
        MhAdapte.Close();
        dispose();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel StatusLable;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    // End of variables declaration//GEN-END:variables

    @Override
    public void tableChanged(TableModelEvent e) {
        if(e.getType() != TableModelEvent.INSERT && e.getLastRow() == tableMode.getRowCount() - 1)
        {
            tableMode.addRow(new Object[tableMode.getColumnCount()]);
        }
        if(e.getType() == TableModelEvent.UPDATE)
        {
            if(e.getColumn() == 0)
            {
                MhAdapte.other = " and mathang.MaMH = " + tableMode.getValueAt(e.getLastRow(), 0).toString();
                MhAdapte.Read();
                if(MhAdapte.table.isEmpty())
                {
                    tableMode.setValueAt("", e.getLastRow(), 2);
                    tableMode.setValueAt("", e.getLastRow(), 3);
                    tableMode.setValueAt("", e.getLastRow(), 4);
                    tableMode.setValueAt("Hàng không tồn tại", e.getLastRow(), 5);
                    SetStatus("Lỗi: Không tìm thấy mặt hàng", false);
                }
                else{
                    Row r = MhAdapte.table.get(0);
                    tableMode.setValueAt(r.GetValue(0), e.getLastRow(), 2);
                    tableMode.setValueAt(r.GetValue(1), e.getLastRow(), 3);
                    tableMode.setValueAt(r.GetValue(2), e.getLastRow(), 4);
                    tableMode.setValueAt("Sẵn Hàng", e.getLastRow(), 5);
                    tableMode.setValueAt(1, e.getLastRow(), 1);
                    SetStatus("", true);
                }
            }
            else if(e.getColumn() == 1){
                long SoLuong = 0;
                long TonKho = 0;
                try{
                    SoLuong = Long.parseLong(tableMode.getValueAt(e.getLastRow(), 1).toString());
                    TonKho = Long.parseLong(tableMode.getValueAt(e.getLastRow(), 3).toString());
                    SetStatus("", true);
                }
                catch(NumberFormatException ex){
                    SetStatus("Lỗi: Chuyển Đổi Số Lượng", false);
                }
                if(SoLuong <= 0)
                    tableMode.setValueAt(1, e.getLastRow(), 1);
                if(SoLuong <= TonKho)
                {
                    tableMode.setValueAt("Khả Thi", e.getLastRow(), 5);
                    SetStatus("", true);
                }
                else{
                    tableMode.setValueAt("Kho không đủ", e.getLastRow(), 5);
                    SetStatus("Lỗi: Yêu cầu quá số lượng hàng trong kho", false);
                }
                TinhTien();
            }
        }
        if(e.getType() == TableModelEvent.DELETE)
            TinhTien();
    }
    private void TinhTien()
    {
        long TongTien = 0;
        for(int i =0; i < tableMode.getRowCount();i++)
            if(tableMode.getValueAt(i, 5) != null && "Khả Thi".equals(tableMode.getValueAt(i, 5).toString()))
            {
                long SoLuong = Long.parseLong(tableMode.getValueAt(i, 1).toString());
                long DonGia = Long.parseLong(tableMode.getValueAt(i, 2).toString());
                TongTien += SoLuong * DonGia;
            }
        jTextField3.setText("" + TongTien);
        TienNhanEditer(null);
    }
    private  void SetStatus(String Message, boolean B)
    {
        StatusLable.setText(Message);
        if(B)
            StatusLable.setForeground(Color.green);
        else
            StatusLable.setForeground(Color.red);
    }
}
