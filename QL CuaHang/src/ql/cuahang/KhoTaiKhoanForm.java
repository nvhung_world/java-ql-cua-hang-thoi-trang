package ql.cuahang;
import MySQL.Connect;
import MySQL.SelectQuery;
import MySQL.Row;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
public class KhoTaiKhoanForm extends javax.swing.JFrame {
    
    public boolean isClose;
    private Connect conn;
    private SelectQuery adapte;
    private DefaultTableModel tableMode;
    private TaiKhoanForm TK;
    public KhoTaiKhoanForm() {
        initComponents();
    }
    public KhoTaiKhoanForm(Connect conn) {
        initComponents();
        setLocationRelativeTo(null);
        isClose = false;
        this.conn = conn;
        tableMode = (DefaultTableModel)JTable.getModel();
        adapte = new SelectQuery(conn);
        SetupAdapte();
    }
    private void SetupAdapte()
    {
        adapte.select.add("TaiKhoan");
        adapte.select.add("concat(Ho, ' ', ten) as 'HoTen'");
        adapte.select.add("Quyen");
        adapte.select.add("GioiTinh");
        adapte.select.add("SoCMT");
        adapte.select.add("SoDT");
        adapte.select.add("thongtintk.Email");
        adapte.select.add("DiaChi");
        adapte.from.add("taikhoan");
        adapte.from.add("thongtintk");
        adapte.where.add("taikhoan.MaTK = thongtintk.MaTK");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        TextTaiKhoan = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        TextHoTen = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TextQuyen = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        TextGioiTinh = new javax.swing.JComboBox<>();
        TimKiem = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTable = new javax.swing.JTable();
        ChinhSua = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Kho Tài Khoản");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Tài Khoản");

        jLabel2.setText("Họ tên");

        jLabel3.setText("Quyền");

        TextQuyen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất Cả", "Khách Hàng", "Nhân Viên", "Quản Trị Viên", "Ngưng Hoạt Động" }));

        jLabel4.setText("Giới Tính");

        TextGioiTinh.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất Cả", "Nam", "Nữ", "Không Dõ", " " }));

        TimKiem.setText("Tìm Kiếm");
        TimKiem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TimKiemClick(evt);
            }
        });

        JTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tài khoản", "Họ tên", "Quyền", "Giới Tính", "So CMT", "Số ĐT", "Email", "Địa chỉ"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(JTable);

        ChinhSua.setText("Chỉnh Sửa");
        ChinhSua.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ChinhSuaClick(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TextTaiKhoan, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TextHoTen, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TextQuyen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TextGioiTinh, 0, 89, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(TimKiem))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(ChinhSua, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(TextTaiKhoan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(TextHoTen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(TextQuyen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(TextGioiTinh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TimKiem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(ChinhSua)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Close();
    }//GEN-LAST:event_formWindowClosing

    private void TimKiemClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TimKiemClick
        adapte.other = "";
        if(!"".equals(TextTaiKhoan.getText()))
            adapte.other += " and upper(TaiKhoan) = upper('" + TextTaiKhoan.getText() +"')";
        if(!"".equals(TextHoTen.getText()))
            adapte.other += " and upper((concat(Ho, ' ', ten)) like upper(N'" + TextHoTen.getText() +"')";
        if(TextQuyen.getSelectedIndex() > 0)
            adapte.other += " and Quyen  = '" + TextQuyen.getModel().getSelectedItem().toString() +"'";
        if(TextGioiTinh.getSelectedIndex() > 0)
            adapte.other += " and GioiTinh = '" + TextGioiTinh.getModel().getSelectedItem().toString() +"'";
        while(tableMode.getRowCount() > 0)
        {            
            tableMode.removeRow(0);
        }
        LoadData();
    }//GEN-LAST:event_TimKiemClick

    private void ChinhSuaClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ChinhSuaClick
        if(TK == null || TK.isClose)
        {
            TK = new TaiKhoanForm(conn);
            TK.setVisible(true);
        }
        else 
            TK.Close();
    }//GEN-LAST:event_ChinhSuaClick
    private  void LoadData()
    {
        adapte.Read();
        for(Row r : adapte.table){
            tableMode.addRow(r.GetAllValue());
        }
    }
    private void SetStatus(String Message, boolean  Error)
    {
        //StatusText.setText(Message);
        //if(Error)
            //StatusText.setForeground(Color.red);
        //else
            //StatusText.setForeground(Color.green);
    }
    public void Close()
    {
        isClose = true;
        adapte.Close();
        dispose();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ChinhSua;
    private javax.swing.JTable JTable;
    private javax.swing.JComboBox<String> TextGioiTinh;
    private javax.swing.JTextField TextHoTen;
    private javax.swing.JComboBox<String> TextQuyen;
    private javax.swing.JTextField TextTaiKhoan;
    private javax.swing.JButton TimKiem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
