package ql.cuahang;
import MySQL.Connect;
import MySQL.SelectQuery;
import MySQL.Row;
import java.awt.Color;
import javax.swing.JOptionPane;
public class MatHangFrom extends javax.swing.JFrame {
    boolean isClose;
    Connect conn;
    SelectQuery adapte;
    public MatHangFrom() {
        initComponents();
        setLocationRelativeTo(null);
    }
    public MatHangFrom(Connect conn)
    {
        initComponents();
        setLocationRelativeTo(null);
        this.conn = conn;
        isClose = false;
        adapte = new SelectQuery(conn);
        CreateAdapte();
    }
    private void CreateAdapte()
    {
        adapte.select.add("mathang.MaMH");
        adapte.select.add("TenMH");
        adapte.select.add("ThuongHieu");
        adapte.select.add("DoiTuong");
        adapte.select.add("loai");
        adapte.select.add("TonKho");
        adapte.select.add("GiaBan");
        adapte.select.add("Nam");
        adapte.select.add("DanhGia");
        adapte.select.add("LuotXem");
        adapte.select.add("MoTaNgan");
        //.....................................................
        adapte.from.add("mathang");
        adapte.from.add("thongtinmh");
        //.....................................................
        adapte.where.add("mathang.MaMH = thongtinmh.MaMH");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        StatusLable = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Thông tin mặt hàng");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jButton1.setText("Update");
        jButton1.setEnabled(false);
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                UpdateClick(evt);
            }
        });

        jButton2.setText("Insert");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InsertClick(evt);
            }
        });

        jButton4.setText("Close");
        jButton4.setToolTipText("");
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CloseClick(evt);
            }
        });

        jLabel1.setText("Tìm Kiếm Mã mặt hàng");

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                MaMHKeyReleased(evt);
            }
        });

        jLabel2.setText("Tên mặt hàng");

        jLabel3.setText("Thương Hiệu");

        jLabel4.setText("DoiTuong");

        jLabel5.setText("loai");

        jTextField6.setEnabled(false);

        jLabel6.setText("TonKho");

        jLabel7.setText("GiaBan");

        jLabel9.setText("Mô tả ngắn");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel8.setText("Nam");

        jLabel10.setText("DanhGia");

        jLabel11.setText("LuotXem");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField2)
                                    .addComponent(jTextField3)
                                    .addComponent(jTextField4)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jTextField7, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jTextField10, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField9, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextField8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                .addContainerGap())
        );

        StatusLable.setText("Status");

        jButton3.setText("Delete");
        jButton3.setToolTipText("");
        jButton3.setEnabled(false);
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DeleteClick(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(StatusLable)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addGap(18, 18, 18)
                .addComponent(jButton4)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(StatusLable)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton4)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void MaMHKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MaMHKeyReleased
        adapte.other = " and mathang.MaMH = " + jTextField1.getText();
        adapte.Read();
        if(adapte.table.isEmpty())
        {
            jButton1.setEnabled(false);
            jButton3.setEnabled(false);
            jButton2.setEnabled(true);
            Clear();
        }
        else
        {
            jButton1.setEnabled(true);
            jButton3.setEnabled(true);
            jButton2.setEnabled(false);
            Row r = adapte.table.get(0);
            jTextField1.setText(r.GetValue(0).toString());
            jTextField2.setText(r.GetValue(1).toString());
            jTextField3.setText(r.GetValue(2).toString());
            jTextField4.setText(r.GetValue(3).toString());
            jTextField5.setText(r.GetValue(4).toString());
            jTextField6.setText(r.GetValue(5).toString());
            jTextField7.setText(r.GetValue(6).toString());
            jTextField7.setText(r.GetValue(7).toString());
            jTextField7.setText(r.GetValue(8).toString());
            jTextField7.setText(r.GetValue(9).toString());
            jTextArea1.setText(r.GetValue(10).toString());
        }
    }//GEN-LAST:event_MaMHKeyReleased
    private void Clear()
    {
        jTextField2.setText("");
        jTextField3.setText("");
        jTextField4.setText("");
        jTextField5.setText("");
        jTextField6.setText("");
        jTextField7.setText("");
        jTextField8.setText("");
        jTextField9.setText("");
        jTextField10.setText("");
        jTextArea1.setText("");
    }
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Close();
    }//GEN-LAST:event_formWindowClosing

    private void CloseClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CloseClick
        Close();
    }//GEN-LAST:event_CloseClick

    private void InsertClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InsertClick
        String TenMH = jTextField2.getText(),
            ThuongHieu = jTextField3.getText(), 
            DoiTuong = jTextField4.getText(), 
            Loai = jTextField5.getText(),
            GiaBan = jTextField7.getText(),
            Nam = jTextField8.getText(),
            DanhGia = jTextField9.getText(),
            LuotXem = jTextField10.getText(),
            MoTa = jTextArea1.getText();
        String Loi = KiemTra(TenMH, Loai, GiaBan, DoiTuong, Nam, DanhGia, LuotXem);
        if("".equals(Loi))
        {
            String[] Query = { "INSERT INTO mathang (TenMH,Loai, GiaBan) VALUES (N'" + 
                    TenMH +
                    "', N'" 
                    + Loai +
                    "', "+ GiaBan + 
                    ");", 
                " SET @last_id_in_table1 = LAST_INSERT_ID();", 
                " INSERT INTO thongtinmh (MaMH ,ThuongHieu , DoiTuong, Nam, DanhGia, LuotXem, MoTaNgan) VALUES (@last_id_in_table1, N'"+ 
                    ThuongHieu +
                    "', N'"+DoiTuong +
                    "', " + Nam +
                    ", " + DanhGia +
                    ", " + LuotXem +
                    ", N'"+ MoTa +"');"};
            System.out.println(Query[2]);
            if(conn.MultiExecute(Query) != null)
            {
                StatusLable.setText("Thành Công");
                StatusLable.setForeground(Color.green);
            }
            else
            {
                StatusLable.setText("Lỗi: không xác định");
                StatusLable.setForeground(Color.red);
            }
        }
        else {
            StatusLable.setText("Lỗi: " + Loi);
            StatusLable.setForeground(Color.red);
            MaMHKeyReleased(null);
        }
    }//GEN-LAST:event_InsertClick

    private void UpdateClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_UpdateClick
        String MaMH = jTextField1.getText(),
            TenMH = jTextField2.getText(),
            ThuongHieu = jTextField3.getText(), 
            DoiTuong = jTextField4.getText(), 
            Loai = jTextField5.getText(), 
            TonKho = jTextField6.getText(), 
            GiaBan = jTextField7.getText(), 
            Nam = jTextField8.getText(),
            DanhGia = jTextField9.getText(),
            LuotXem = jTextField10.getText(),
            MoTa = jTextArea1.getText();
        String Loi = KiemTra(TenMH, Loai, GiaBan, DoiTuong, Nam, DanhGia, LuotXem);
        if("".equals(Loi))
        {
            String[] Query = { "UPDATE mathang SET TenMH = N'" + 
                    TenMH + 
                    "',Loai = N'"+ 
                    Loai + 
                    "', GiaBan = " 
                    + GiaBan +
                    " WHERE MaMH = "+
                    MaMH+
                    ";",
                "UPDATE thongtinmh SET ThuongHieu = N'"+ ThuongHieu + 
                    "', DoiTuong = N'" + DoiTuong +
                    "', Nam = " + Nam +
                    ", DanhGia = " + DanhGia +
                    ", LuotXem = " + LuotXem +
                    ", MoTaNgan = N'"+ MoTa +
                    "' WHERE MaMH = "+ MaMH +";"
            };
            if(conn.MultiExecute(Query) != null)
            {
                StatusLable.setText("Thành Công");
                StatusLable.setForeground(Color.green);
                MaMHKeyReleased(null);
            }
            else
            {
                StatusLable.setText("Lỗi: không xác định");
                StatusLable.setForeground(Color.red);
            }
        }
        else
        {
            StatusLable.setText("Lỗi: " + Loi);
            StatusLable.setForeground(Color.red);
        }
    }//GEN-LAST:event_UpdateClick

    private void DeleteClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DeleteClick
        int result = JOptionPane.showConfirmDialog(
                this,
                "Bạn thực sự muốn xóa mặt hàng " + jTextField2.getText() +" ?",
                "Xóa Mặt Hàng",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        if(result == JOptionPane.YES_OPTION){
            if(conn.executeUpdate("DELETE FROM mathang WHERE MaMH = " + jTextField1.getText()) != -1)
            {
                StatusLable.setText("Thành Công");
                StatusLable.setForeground(Color.green);
                MaMHKeyReleased(null);
            }
            else
            {
                StatusLable.setText("Lỗi: không xác định");
                StatusLable.setForeground(Color.red);
            }
        }
    }//GEN-LAST:event_DeleteClick
    private String KiemTra(String TenMH, String Loai, String GiaBan, String DoiTuong, String Nam, String DanhGia, String LuotXem)
    {
        if("".equals(TenMH))
            return "Tên mặt hàng không được bỏ trống";
        else if("".equals(Loai))
            return "Loại mặt hàng không được bỏ trống";
        else if("".equals(DoiTuong))
            return "Đối tượng không được bỏ trống";
        try{
            Integer.parseInt(GiaBan);
        }catch(NumberFormatException ex)
        {
            return "Giá bán phải là số nguyên";
        }
        try{
            Integer.parseInt(Nam);
        }catch(NumberFormatException ex)
        {
            return "Năm phải là số nguyên";
        }
        try{
            Integer.parseInt(DanhGia);
        }catch(NumberFormatException ex)
        {
            return "Đánh Giá phải là số nguyên";
        }
        try{
            Integer.parseInt(LuotXem);
        }catch(NumberFormatException ex)
        {
            return "Lượt xem phải là số nguyên";
        }
        return "";
    }
    public void Close()
    {
        isClose = true;
        adapte.Close();
        dispose();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel StatusLable;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
