package ql.cuahang;
import MySQL.Connect;
import MySQL.SelectQuery;
import MySQL.Row;
import javax.swing.JOptionPane;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class TaiKhoanForm extends javax.swing.JFrame {
    public boolean isClose;
    private Connect conn;
    private SelectQuery adapte;
    public TaiKhoanForm() {
        initComponents();
    }
    public TaiKhoanForm(Connect conn) {
        initComponents();
        setLocationRelativeTo(null);
        isClose = false;
        this.conn = conn;
        adapte = new SelectQuery(conn);
        SetupAdapte();
    }
    private void SetupAdapte()
    {
        adapte.select.add("MatKhau");
        adapte.select.add("MaCap2");
        adapte.select.add("Quyen");
        adapte.select.add("taikhoan.Email");
        adapte.select.add("Ho");
        adapte.select.add("ten");
        adapte.select.add("GioiTinh");
        adapte.select.add("NgaySinh");
        adapte.select.add("SoCMT");
        adapte.select.add("SoDT");
        adapte.select.add("thongtintk.Email");
        adapte.select.add("DiaChi");
        adapte.from.add("taikhoan");
        adapte.from.add("thongtintk");
        adapte.where.add("taikhoan.MaTK = thongtintk.MaTK");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        TextTK = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        TextMK = new javax.swing.JTextField();
        TextMK2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        TextQ = new javax.swing.JComboBox<>();
        TextEBV = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TextH = new javax.swing.JTextField();
        TextT = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        TextGT = new javax.swing.JComboBox<>();
        TextNS = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        TextCMT = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        TextELH = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        TextDT = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TextDC = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        ButtonDe = new javax.swing.JButton();
        ButtonUp = new javax.swing.JButton();
        ButtonIN = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Chỉnh Sửa Tài Khoản");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Tìm kiếm tài khoản");

        TextTK.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TaiKhoanEditer(evt);
            }
        });

        jLabel2.setText("Mật khẩu");

        jLabel3.setText("Mật khẩu 2");

        jLabel4.setText("Quyền");

        TextQ.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Khách Hàng", "Nhân Viên", "Quản Trị Viên", "Ngưng Hoạt Động" }));

        jLabel5.setText("Email bảo vệ");

        jLabel6.setText("Họ");

        jLabel7.setText("Tên");

        jLabel8.setText("Giới tính");

        TextGT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Không Dõ", "Nam", "Nữ" }));

        jLabel9.setText("Ngày sinh");

        jLabel10.setText("Số CMT");

        jLabel12.setText("Số ĐT");

        jLabel13.setText("Email liên hệ");

        jLabel11.setText("Địa chỉ");

        TextDC.setColumns(20);
        TextDC.setRows(5);
        jScrollPane1.setViewportView(TextDC);

        jButton1.setText("Close");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CloseClick(evt);
            }
        });

        ButtonDe.setText("Delete");
        ButtonDe.setEnabled(false);
        ButtonDe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DeleteClick(evt);
            }
        });

        ButtonUp.setText("Update");
        ButtonUp.setEnabled(false);
        ButtonUp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                UpdateClick(evt);
            }
        });

        ButtonIN.setText("Insert");
        ButtonIN.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InsertClick(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TextELH, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel11))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane1))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGap(56, 56, 56)
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(TextTK, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addContainerGap()
                                                    .addComponent(jLabel2))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addGap(28, 28, 28)
                                                        .addComponent(jLabel6))
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addContainerGap()
                                                        .addComponent(jLabel3))
                                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                        .addContainerGap()
                                                        .addComponent(jLabel8))))
                                            .addGap(18, 18, 18))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel10)
                                            .addGap(23, 23, 23)))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(TextMK)
                                                .addComponent(TextMK2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                                .addComponent(TextH, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(TextGT, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGap(34, 34, 34)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel4)
                                                        .addComponent(jLabel7)))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGap(18, 18, 18)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel5)))))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(TextCMT, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel12)))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(TextDT)
                                        .addComponent(TextQ, 0, 132, Short.MAX_VALUE)
                                        .addComponent(TextEBV)
                                        .addComponent(TextT)
                                        .addComponent(TextNS)))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(ButtonUp)
                        .addGap(18, 18, 18)
                        .addComponent(ButtonIN)
                        .addGap(18, 18, 18)
                        .addComponent(ButtonDe)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(TextTK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(TextMK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(TextQ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(TextMK2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(TextEBV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TextH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(TextT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(TextGT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TextNS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(TextCMT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(TextDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(TextELH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(ButtonIN)
                        .addComponent(ButtonUp))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(ButtonDe)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Close();
    }//GEN-LAST:event_formWindowClosing
    private void Clear()
    {
        TextMK.setText("");
        TextMK2.setText("");
        TextQ.setSelectedIndex(0);
        TextEBV.setText("");
        TextT.setText("");
        TextH.setText("");
        TextGT.setSelectedIndex(0);
        TextNS.setText("");
        TextCMT.setText("");
        TextDT.setText("");
        TextELH.setText("");
        TextDC.setText("");
    }
    private void TaiKhoanEditer(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TaiKhoanEditer
        String TaiKhoan = TextTK.getText();
        adapte.other = " and TaiKhoan = N'" + TaiKhoan + "'";
        adapte.Read();
        if(adapte.table.isEmpty())
        {
            Clear();
            ButtonUp.setEnabled(false);
            ButtonDe.setEnabled(false);
            ButtonIN.setEnabled(true);
        }
        else
        {
            ButtonUp.setEnabled(true);
            ButtonDe.setEnabled(true);
            ButtonIN.setEnabled(false);
            Row r = adapte.table.get(0);
            TextMK.setText(r.GetValue(0).toString());
            TextMK2.setText(r.GetValue(1).toString());
            TextQ.setSelectedItem(r.GetValue(2).toString());
            TextEBV.setText(r.GetValue(3).toString());
            TextH.setText(r.GetValue(4).toString());
            TextT.setText(r.GetValue(5).toString());
            TextGT.setSelectedItem(r.GetValue(6).toString());
            if(r.GetValue(7) != null)
                TextNS.setText(r.GetValue(7).toString());
            else
                TextNS.setText("");
            TextCMT.setText(r.GetValue(8).toString());
            TextDT.setText(r.GetValue(9).toString());
            TextELH.setText(r.GetValue(10).toString());
            if(r.GetValue(11) != null)
                TextDC.setText(r.GetValue(11).toString());
            else
                TextDC.setText("");
        }
    }//GEN-LAST:event_TaiKhoanEditer

    private void CloseClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CloseClick
        Close();
    }//GEN-LAST:event_CloseClick

    private void DeleteClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DeleteClick
        int result = JOptionPane.showConfirmDialog(
                this,
                "Bạn thực sự muốn xóa tải khoản " + TextTK.getText() +" ?",
                "Xóa Tài Khoản",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        if(result == JOptionPane.YES_OPTION){
            if(conn.executeUpdate("DELETE FROM taikhoan WHERE TaiKhoan = N'" + TextTK.getText() + "'") != -1)
            {
                TaiKhoanEditer(null);
            }
            else
            {
            }
        }
    }//GEN-LAST:event_DeleteClick

    private void InsertClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InsertClick
        if(KiemTra())
        {
            String[] Query = {"INSERT INTO taikhoan(TaiKhoan,MatKhau,Quyen,NgayLap,MaCap2,Email)VALUES(N'" + 
                    TextTK.getText() +
                    "', '" + TextMK.getText() +
                    "', '" + TextQ.getSelectedItem().toString() +
                    "', '" + LocalDate.now().format(DateTimeFormatter.ISO_DATE) +
                    "', '" + TextMK2.getText() +
                    "', N'" + TextEBV.getText() +
                    "');",
                " SET @last_id_in_table1 = LAST_INSERT_ID();",
                " INSERT INTO thongtintk(MaTK,Ho,Ten,GioiTinh" + ("".equals(TextNS.getText())? "": ",NgaySinh") + ",SoCMT,Email,SoDT,DiaChi)VALUES( @last_id_in_table1, N'"+
                    TextH.getText() +
                    "', N'" + TextT.getText() +
                    "', '" + TextGT.getSelectedItem().toString() +
                    ("".equals(TextNS.getText())? "":  "', '" + TextNS.getText()) +
                    "', '" + TextCMT.getText() +
                    "', '" + TextELH.getText() +
                    "', '" + TextDT.getText() +
                    "', '" + TextDC.getText() +
                    "');"};
            if(conn.MultiExecute(Query) != null)
            {
                TaiKhoanEditer(null);
            }
            else
            {
                JOptionPane.showConfirmDialog(
                    this,
                    "Không thành công",
                    "Lỗi",
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_InsertClick

    private void UpdateClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_UpdateClick
        if(KiemTra())
        {
            String[] Query = {"UPDATE taikhoan SET" + 
                    " MatKhau = '" + TextMK.getText()+
                    "', Quyen = '" + TextQ.getSelectedItem().toString() +
                    "', MaCap2 = '" + TextMK2.getText() +
                    "', Email = '" + TextEBV.getText()+ 
                    "' WHERE " +
                    " TaiKhoan = '" + TextTK.getText() +"';",
                " update thongtintk a RIGHT join taikhoan b on a.MaTK = b.MaTK set " +
                    " ho = '" + TextH.getText() +"', " +
                    " ten = '" + TextT.getText() +"', " +
                    " gioitinh = '" + TextGT.getSelectedItem().toString() +"', " +
                    ("".equals(TextNS.getText()) ? "": " ngaysinh = '" + TextNS.getText() +"', " ) +
                    " SoCMT = '" + TextCMT.getText() +"', " +
                    " SoDT = '" + TextDT.getText() +"', " +
                    " DiaChi = '" + TextDC.getText() +"', " +
                    " b.Email = '" + TextELH.getText() +"' " +
                    "where TaiKhoan = '" + TextTK.getText() +"'"};
            if(conn.MultiExecute(Query) != null)
            {
                TaiKhoanEditer(null);
            }
            else
            {
                JOptionPane.showConfirmDialog(
                    this,
                    "Không thành công",
                    "Lỗi",
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_UpdateClick
    private boolean KiemTra()
    {
        return true;
    }
    public void Close()
    {
        isClose = true;
        adapte.Close();
        dispose();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonDe;
    private javax.swing.JButton ButtonIN;
    private javax.swing.JButton ButtonUp;
    private javax.swing.JTextField TextCMT;
    private javax.swing.JTextArea TextDC;
    private javax.swing.JTextField TextDT;
    private javax.swing.JTextField TextEBV;
    private javax.swing.JTextField TextELH;
    private javax.swing.JComboBox<String> TextGT;
    private javax.swing.JTextField TextH;
    private javax.swing.JTextField TextMK;
    private javax.swing.JTextField TextMK2;
    private javax.swing.JTextField TextNS;
    private javax.swing.JComboBox<String> TextQ;
    private javax.swing.JTextField TextT;
    private javax.swing.JTextField TextTK;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
