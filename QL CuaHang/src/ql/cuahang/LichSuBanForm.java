package ql.cuahang;
import MySQL.Connect;
import MySQL.SelectQuery;
import MySQL.Row;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
public class LichSuBanForm extends javax.swing.JFrame {
    public boolean isClose;
    Connect conn;
    private SelectQuery adapte;
    private DefaultTableModel tableMode;
    public LichSuBanForm() {
        initComponents();
    }
    public LichSuBanForm(Connect conn) {
        initComponents();
        setLocationRelativeTo(null);
        isClose = false;
        this.conn = conn;
        tableMode = (DefaultTableModel)JTable.getModel();
        adapte = new SelectQuery(conn);
        SetupAdapte();
    }
    void SetupAdapte()
    {
        adapte.select.add("TaiKhoan");
        adapte.select.add("concat(Ho, ' ', Ten) as 'hoTen'");
        adapte.select.add("GioiTinh");
        adapte.select.add("mathang.MaMH");
        adapte.select.add("TenMH");
        adapte.select.add("Loai");
        adapte.select.add("donhang.NgayBan");
        adapte.select.add("chitietdh.SoLuong");
        adapte.select.add("ThuongHieu");
        adapte.select.add("DoiTuong");
        adapte.select.add("TrangThai");
        adapte.from.add("taikhoan");
        adapte.from.add("thongtintk");
        adapte.from.add("donhang");
        adapte.from.add("chitietdh");
        adapte.from.add("mathang");
        adapte.from.add("thongtinmh");
        adapte.where.add("taikhoan.MaTK = thongtintk.MaTK");
        adapte.where.add("taikhoan.MaTK = donhang.MaTK");
        adapte.where.add("donhang.MaDH = chitietdh.MaDH");
        adapte.where.add("chitietdh.MaMH = mathang.MaMH");
        adapte.where.add("mathang.MaMH = thongtinmh.MaMH");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        TextTK = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        TextHT = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TextMMH = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        TextMH = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        TextTH = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        TextDT = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        TextGT = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        TextLH = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        TextTT = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Lịch sử mua bán");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Tài Khoản");

        jLabel2.setText("Họ Tên");

        jLabel3.setText("Mã Mặt Hàng");

        jLabel4.setText("Tên Mặt Hàng");

        jLabel5.setText("Thương Hiệu");

        jLabel6.setText("Đối Tượng");

        jLabel7.setText("Giới Tính");

        TextGT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả", "Nam", "Nữ", "Không Dõ" }));

        jLabel8.setText("Loại");

        jLabel9.setText("Trạng Thái Đơn Hàng");

        jButton1.setText("Tìm Kiếm");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TimKiemClick(evt);
            }
        });

        TextTT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả", "Giỏ Hàng", "Đang Chuyển", "Đã Thanh Toán" }));

        JTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tài Khoản", "Họ Tên", "Giới Tính", "Mã Hàng", "Tên Hàng", "Loại Hàng", "Ngày Bán", "Số Lượng", "Thương Hiệu", "Đối Tượng", "Trạng Thái"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(JTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(TextGT, 0, 147, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(TextTK, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
                                    .addComponent(TextHT))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabel8)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(TextLH)
                            .addComponent(TextMH)
                            .addComponent(TextMMH, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(TextTT, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addGap(33, 33, 33)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(TextDT)
                                    .addComponent(TextTH, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(42, 42, 42)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(14, 14, 14)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(TextTK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(TextHT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7)
                                .addComponent(TextGT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel5)
                                        .addComponent(TextTH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6)
                                        .addComponent(TextDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel9)
                                        .addComponent(TextTT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(TextMMH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(TextMH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(TextLH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel8))))))
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void SetStatus(String Message, boolean  Error)
    {
        //StatusText.setText(Message);
        //if(Error)
            //StatusText.setForeground(Color.red);
        //else
            //StatusText.setForeground(Color.green);
    }
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Close();
    }//GEN-LAST:event_formWindowClosing

    private void TimKiemClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TimKiemClick
        adapte.other = "";
        if(!"".equals(TextTK.getText()))
            adapte.other += " and upper(TaiKhoan) = upper('" + TextTK.getText() +"')";
        if(!"".equals(TextHT.getText()))
            adapte.other += " and upper(concat(Ho, ' ', ten)) like upper('%" + TextHT.getText() +"%')";
        if(!"".equals(TextMMH.getText()))
            adapte.other += " and mathang.MaMH = '" + TextMMH.getText() +"'";
        if(!"".equals(TextMH.getText()))
            adapte.other += " and upper(TenMH) like upper('%" + TextMH.getText() +"%')";
        if(!"".equals(TextLH.getText()))
            adapte.other += " and upper(Loai) like upper('%" + TextLH.getText() +"%')";
        if(!"".equals(TextTH.getText()))
            adapte.other += " and upper(ThuongHieu) like upper('%" + TextTH.getText() +"%')";
        if(!"".equals(TextDT.getText()))
            adapte.other += " and upper(DoiTuong) like upper('%" + TextDT.getText() +"%')";
        if(TextGT.getSelectedIndex() > 0)
            adapte.other += " and GioiTinh like '" + TextGT.getModel().getSelectedItem().toString() +"'";
        if(TextTT.getSelectedIndex() > 0)
            adapte.other += " and TrangThai like '" + TextTT.getModel().getSelectedItem().toString() +"'";
        while(tableMode.getRowCount() > 0)
        {            
            tableMode.removeRow(0);
        }
        // Load Data
        LoadData();
    }//GEN-LAST:event_TimKiemClick
    private  void LoadData()
    {
        adapte.Read();
        for(Row r : adapte.table){
            tableMode.addRow(r.GetAllValue());
        }
    }
    public void Close()
    {
        isClose = true;
        adapte.Close();
        dispose();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable JTable;
    private javax.swing.JTextField TextDT;
    private javax.swing.JComboBox<String> TextGT;
    private javax.swing.JTextField TextHT;
    private javax.swing.JTextField TextLH;
    private javax.swing.JTextField TextMH;
    private javax.swing.JTextField TextMMH;
    private javax.swing.JTextField TextTH;
    private javax.swing.JTextField TextTK;
    private javax.swing.JComboBox<String> TextTT;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
