package MySQL;
import java.util.ArrayList;
public class Row 
{
    private ArrayList<String> collum;
    private Object value[];
    public Row(ArrayList<String> collum)
    {
        this.collum = collum;
        value = new Object[collum.size()];
    }
    public boolean SetValue(String CollumName, Object value)
    {
        int index = collum.indexOf(CollumName);
        if(index >=0 && index < Cout())
        {
            this.value[index] = value;
            return true;
        }
        else
            return false;
    }
    public boolean SetValue(int index, Object value)
    {
        if(index >=0 && index < Cout())
        {
            this.value[index] = value;
            return true;
        }
        else
            return false;
    }
    public Object GetValue(String CollumName)
    {
        int index = collum.indexOf(CollumName);
        if(index >=0 && index < Cout())
            return this.value[index];
        else
            return null;
    }
    public Object GetValue(int index)
    {
        if(index >=0 && index < Cout())
            return this.value[index];
        else
            return null;
    }
    public Object[] GetAllValue()
    {
        return value;
    }
    public int Cout()
    {
        return collum.size();
    }
}
