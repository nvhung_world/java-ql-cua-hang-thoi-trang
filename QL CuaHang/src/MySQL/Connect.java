package MySQL;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Connect {
    Connection conn = null;
    String DiaChiDB="jdbc:mysql://127.0.0.1:3307/fashionshop",
            User="root",
            Pass="hungit";
    Statement stmt;
    public Connect()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
        }
    }
    public boolean Open()
    {
        try {
            conn = DriverManager.getConnection(DiaChiDB,User, Pass);
            stmt = conn.createStatement();
            return true;
        } 
        catch (SQLException ex) 
        {
            return false;
        }
    }
    public Statement CreateStatement(int a, int b)
    {
        try{
            return conn.createStatement(a, b);
        }
        catch(SQLException ex)
        {
            return null;
        }
    }
    public ResultSet executeQuery(String query) //Lay ket qua
    {
        try{
            return stmt.executeQuery(query);
        }
        catch(SQLException ex){
            return null;
        }
    }
    public int[] MultiExecute(String query[]) // Thuc thi cau lenh SQL
    {
        try{
            for(String S : query)
                stmt.addBatch(S);
            return stmt.executeBatch();
        }
        catch(SQLException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
    public int executeUpdate(String query) // Thuc thi update, delete, insert
    {
        try{
            return stmt.executeUpdate(query);
        }
        catch(SQLException ex){
            return -1;
        }
    }
    public boolean close()
    {
        try{
            stmt.close();
            conn.close();
            return true;
        }catch(SQLException ex){
            return false;
        }
    }
}
