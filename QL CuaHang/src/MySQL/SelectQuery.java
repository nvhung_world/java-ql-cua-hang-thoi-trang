
package MySQL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
public class SelectQuery
{
    boolean isOpen;
    Connect conn;
    ResultSet result;
    public String other = "";
    public String limt = "";
    public ArrayList<String> select, from, where;
    public ArrayList<Row> table;
    public SelectQuery(Connect conn)
    {
        this.conn = conn;
        isOpen = false;
        select = new ArrayList<>();
        from = new ArrayList<>();
        where = new ArrayList<>();
        table = new ArrayList<>();
    }
    private String CreateQuery()
    {
        String S = "";
        // Tạo Select
        if(select.isEmpty())
            S += "select *";
        else
        {
            for (int i = 0; i< select.size(); i++)
                if(i ==0)
                    S +="select " + select.get(i);
                else
                    S +=", " + select.get(i);
        }
        // Tạo From
        for (int i = 0; i< from.size(); i++)
            if(i ==0)
                S +=" from " + from.get(i);
            else
                S +=", " + from.get(i);
        // Tạo WHere
        for (int i = 0; i < where.size(); i++)
            if(i ==0)
                S +=" where " + where.get(i);
            else
                S +=" and " + where.get(i);
        // Tạo other
        S += other + " " + limt;
        return S;
    }
    public void Close()
    {
        try{
            if(isOpen)
                result.close();
        }
        catch(SQLException ex)
        {
        }
    }
    public void Read()
    {
        //System.out.println(CreateQuery());
        result = conn.executeQuery(CreateQuery());
        if(result != null)
        {
            isOpen = true;
            table.clear();
            try
            {
                while(result.next()){
                    Row r = new Row(select);
                    for(int i = 0; i < select.size(); i++)
                        r.SetValue(i, result.getObject(i+1));
                    table.add(r);
                }
            }
            catch(SQLException ex)
            {
            }
        }
        else
        {
            isOpen = false;
            table.clear();
        }
    }
    public Row CreateRow()
    {
        return new Row(select);
    }
}
